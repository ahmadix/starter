import React from 'react';
import { View, Text } from 'react-native';
import { Navigation } from 'react-native-navigation';

const FirstTabScreen = () => {
    return (<View><Text>First screen</Text></View>)
}
const SecondTabScreen = () => {
    return (<View><Text>Second screen</Text></View>)
}
const PushedScreen = () => {
    return (<View><Text>pushed screen</Text></View>)
}

// register all screens of the app (including internal ones)
function registerScreens() {
    Navigation.registerComponent('example.FirstTabScreen', () => FirstTabScreen);
    Navigation.registerComponent('example.SecondTabScreen', () => SecondTabScreen);
    Navigation.registerComponent('example.PushedScreen', () => PushedScreen);
}
registerScreens(); // this is where you register all of your app's screens

// start the app
Navigation.startTabBasedApp({
    tabs: [
        {
            label: 'One',
            screen: 'example.FirstTabScreen', // this is a registered name for a screen
            icon: require('./navicon_menu.png'),
            // selectedIcon: require('../img/one_selected.png'), // iOS only
            title: 'Screen One'
        },
        {
            label: 'Two',
            screen: 'example.SecondTabScreen',
            icon: require('./navicon_menu.png'),
            // selectedIcon: require('../img/two_selected.png'), // iOS only
            title: 'Screen Two'
        }
    ],
    drawer: { // optional, add this if you want a side menu drawer in your app
        left: { // optional, define if you want a drawer from the left
            screen: 'example.PushedScreen', // unique ID registered with Navigation.registerScreen
            passProps: {} // simple serializable object that will pass as props to all top screens (optional)
        },
        disableOpenGesture: false // optional, can the drawer be opened with a swipe instead of button
    },
});