import { Navigation } from 'react-native-navigation';

import { registerScreens } from './screens';

import { loadIcons } from './icons-generator';

registerScreens();

loadIcons().then(icons => {
    Navigation.startTabBasedApp({
        tabs: [
            {
                label: 'Home',
                screen: 'homeScreen',
                title: 'Stream',
                icon: icons['home']
            },
            {
                label: 'Categories',
                screen: 'categoriesScreen',
                title: 'Categories',
                icon: icons['th-large']
            }
        ]
    });
});
