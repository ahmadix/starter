import Icon from 'react-native-vector-icons/FontAwesome';

export function loadIcons() {
    const icons = [
        { name: 'home', size: 30, color: '#000000' },
        { name: 'th-large', size: 30, color: '#000000' }
    ];

    return Promise.all(icons.map(icon => {
        const { name, size, color } = icon;
        return Icon.getImageSource(name, size, color);
    })).then(sources => new Promise((resolve) => {
        const iconsMap = {};
        icons.forEach((icon, idx) => {
            iconsMap[icon.name] = sources[idx];
        });
        resolve(iconsMap);
    }));
}
